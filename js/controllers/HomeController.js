app.controller('HomeController', ['$scope', '$location', '$route', function($scope, $location, $route) {

    //var chat_div = document.getElementById("tasks");
    $scope.task = {};
    $scope.class_to_add = {};
    $scope.classes = [];
    //console.log("in HomeController");

    ////////////////////// create socket events ///////////////////////////////
    // else {
    //     // only connect socket when user is logged in
    //     socket = io.connect();

    //     socket.emit('user_connected', { "user": user.getUser(), "room": $scope.current_room });

    //     // socket events
    //     socket.on('chat_message', function(data) {
    //         var message_html;
    //         var divElement = angular.element(document.querySelector('#messages'));

    //         // append new message to chat div		
    //         if (data.uid == user.getUser().uid) {
    //             message_html = "<div class='row'><div class='col-lg-8 col-xs-8 pull-right'><div class='list-group'><div class='list-group-item' style='background-color: #66CCFF'><h4 class='list-group-item-heading'>" + data.sent_by + "</h4><p class='list-group-item-text'>" + data.message + "</p></div></div></div></div>";
    //         } else {
    //             message_html = "<div class='row'><div class='col-lg-8 col-xs-8 pull-left'><div class='list-group'><div class='list-group-item' style='background-color: #99FF99'><h4 class='list-group-item-heading'>" + data.sent_by + "</h4><p class='list-group-item-text'>" + data.message + "</p></div></div></div></div>";
    //         }

    //         divElement.append(message_html);

    //         chat_div = document.getElementById("messages");
    //         chat_div.scrollTop = chat_div.scrollHeight; // scroll div to bottom
    //     });

    // }
    //////////////////////// end socket events ////////////////////////////////////////////////////

    /////////////// send messages /////////////////
    $scope.addTask = function() {
        console.log("in addTask()");

        if (!$scope.task.task || !$scope.task.date)
            return;

        //////////// add task to div //////////////
        var task_html;
        var divElement = angular.element(document.querySelector('#tasks'));

        // build task html
        task_html = "<div class='row'><div class='col-lg-8 col-xs-8 pull-left'>\
                     <div class='list-group'><div class='list-group-item' \
                     style='background-color: " + $scope.task.class.color + "'><h4 class='list-group-item-heading'>\
                     " + $scope.task.class.name + "</h4><h4 class='list-group-item-heading'>\
                     Duedate: " + $scope.task.date + "</h4><p class='list-group-item-text'>\
                     " + $scope.task.task + "</p></div></div></div></div>";

        // add the new task
        divElement.append(task_html);

        // scroll div to bottom
        task_div = document.getElementById("tasks");
        task_div.scrollTop = task_div.scrollHeight;

        // clear out message holder
        $scope.task = {};
    };

    $scope.addClass = function() {
        console.log("in addClass()");

        $scope.classes[$scope.classes.length] = $scope.class_to_add;
        $scope.class_to_add = {};

        console.log($scope.classes);
    };

    $scope.pickClass = function(task_class) {
        console.log(task_class);
        $scope.task.class = task_class;
    };


    ///////////// function to populate the message div ////////////
    // $scope.populate_messages = function() {
    //     if (!messages.checkPopulated()) {

    //         var message_html;
    //         var divElement = angular.element(document.querySelector('#messages'));

    //         // first we need to clear the contents of the chat div
    //         divElement.empty();

    //         divElement.innerHtml = "";

    //         if ($scope.messages) {
    //             for (var i = 0; i < $scope.messages.length; i++) {
    //                 // check if user sent this message
    //                 if ($scope.messages[i].uid == user.getUser().uid) {
    //                     message_html = "<div class='row'><div class='col-lg-8 col-xs-8 pull-right'><div class='list-group'><div class='list-group-item' style='background-color: #66CCFF'><h4 class='list-group-item-heading'>" + $scope.messages[i].sent_by + "</h4><p class='list-group-item-text'>" + $scope.messages[i].message + "</p></div></div></div></div>";
    //                 } else {
    //                     message_html = "<div class='row'><div class='col-lg-8 col-xs-8 pull-left'><div class='list-group'><div class='list-group-item' style='background-color: #99FF99'><h4 class='list-group-item-heading'>" + $scope.messages[i].sent_by + "</h4><p class='list-group-item-text'>" + $scope.messages[i].message + "</p></div></div></div></div>";
    //                 }

    //                 // add message to chat area
    //                 divElement.append(message_html);
    //             }
    //         }

    //         messages.setPopulated(true); // set populated flag so the div won't populate again
    //     }
    // };

}]);